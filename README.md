## Description

Open source pixel art emotes that fits well to Twitch and Discord.

## Size

All emotes are 112x112 sized, scaled from 28x28 or 56x56.
